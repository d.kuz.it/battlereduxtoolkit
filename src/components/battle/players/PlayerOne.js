import {updatePlayerOne} from "../../../State/Battle/battle.slice";
import {useDispatch, useSelector} from "react-redux";
import PlayerPreview from "../PlayerPreview";
import {PlayerInputOne} from "./PlayerInputOne";




const PlayerOne = () => {
    const dispatch = useDispatch()
    const playerDataOne = useSelector(state => state.battle.playerDataOne)


    const handleSubmitOne = (id, userName) => {
        console.log("ONE SUB")
        dispatch(updatePlayerOne({
            playerOneName: userName,
            playerOneImage: `https://github.com/${userName}.png?size200`,

        }))
    }
    const handleReset = (id) => {
        console.log("ONE")
        dispatch(updatePlayerOne({
            playerOneName: "",
            playerOneImage: null
        }))
    }

        return (
            <div className='row'>
                {playerDataOne.playerOneImage ?
                    <PlayerPreview
                        avatar={playerDataOne.playerOneImage}
                        username={playerDataOne.playerOneName}
                    >
                        <button className='reset' onClick={()=> {handleReset('playerOne')}}>
                            reset
                        </button>
                    </PlayerPreview> :
                    <PlayerInputOne
                        id='playerOne'
                        label='player 1'
                        player='One'
                        onSubmit={handleSubmitOne}
                    />}
                </div>
                )
        }


export default PlayerOne;
