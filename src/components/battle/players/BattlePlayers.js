import React from "react";
import {useSelector} from "react-redux";
import {Link} from "react-router-dom";

const BattlePlayers = () => {
    const playerDataOne = useSelector(state => state.battle.playerDataOne)
    const playerDataTwo = useSelector(state => state.battle.playerDataTwo)



    return (
        <div>
            {playerDataOne.playerOneImage && playerDataTwo.playerTwoImage ?
            <Link to={{
                pathname: 'results',
                search: `?playerOneName=${playerDataOne.playerOneName}&playerTwoName=${playerDataTwo.playerTwoName}`
            }} className='button'> battle  </Link>
            :
            null}
        </div>

    )
}

export default BattlePlayers;