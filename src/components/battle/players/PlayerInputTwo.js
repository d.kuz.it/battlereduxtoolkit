import {useDispatch, useSelector} from "react-redux";
import {updatePlayerTwo} from "../../../State/Battle/battle.slice";

export const PlayerInputTwo = ({id, label, onSubmit}) => {
    const dispatch = useDispatch();
    const userName = useSelector(state => state.battle.playerDataTwo)
    const handleSubmit = (event) => {
        event.preventDefault()
        onSubmit(id, userName)
    }


    return (<form className='column' onSubmit={handleSubmit} >
            <label className='header' htmlFor={label}>{label}</label>
            <input
                id={label}
                type='text'
                placeholder='GitHub UserName'
                autoComplete='off'
                value={userName.playerTwoName}
                onChange={(event) =>
                    dispatch(updatePlayerTwo(event.target.value))}/>
            <button className='button'
                    type='submit'
                    // disabled={!userName.length}
            >
                Submit
            </button>
        </form>
    )
}

