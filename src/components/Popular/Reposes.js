import {useSelector} from "react-redux";
import {Loader} from "../../Loader/Loader";
import {Persons} from "./Persons";


export const Reposes = () => {
    const loading = useSelector(state => state.popular.loading)

return <>
    {loading? <Loader /> : <Persons  />}
</>

}
