import  './Loader.css'
export const Loader = (props) => {
  return (

          <div className="loader">
              <div className="loader-inner">
                  <div className="loader-line-wrap">
                      <div className="loader-line"></div>
                  </div>
                  <div className="loader-line-wrap">
                      <div className="loader-line"></div>
                  </div>
                  <div className="loader-line-wrap">
                      <div className="loader-line"></div>
                  </div>
                  <div className="loader-line-wrap">
                      <div className="loader-line"></div>
                  </div>
                  <div className="loader-line-wrap">
                      <div className="loader-line"></div>
                  </div>
              </div>
          </div>
  )
}
