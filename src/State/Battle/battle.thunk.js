import {makeBattle} from "../../api/Api";
import {updateGetLoser, updateGetWinner, updatePlayerLoad} from "./battle.action";
import {createAsyncThunk} from "@reduxjs/toolkit";

// export const getWinner = (playerOne, playerTwo) => (dispatch) => {
//
//
//     makeBattle([playerOne, playerTwo])
//         .then(([winner, loser]) => {
//             dispatch(updateGetWinner(winner));
//             dispatch(updateGetLoser(loser));
//         })
//         .finally(() => dispatch(updatePlayerLoad(false)))
// }
export const getWinner = createAsyncThunk(
    'battle/getWinner',

    async (players, {rejectWithValue},dispatch)=> {
        try {
            console.log("array ", players)
            return await makeBattle(players)
        } catch (error) {
            return rejectWithValue(error)
        }
    }
)