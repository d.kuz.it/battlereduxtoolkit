import {createSlice} from "@reduxjs/toolkit";
import {getWinner} from "./battle.thunk";


const initialState = {
    playerDataOne: {
        playerOneName: "",
        playerOneImage: null,
    },
    playerDataTwo: {
        playerTwoName: "",
        playerTwoImage: null
    },
    winner: {
        profile: [],
        score: null,
    },
    loser: {
        profile: [],
        score: null,
    },
    loading: true,
}


const battleSlice = createSlice({
    name: 'battle',
    initialState,
    reducers: {
        updatePlayerOne : (state, action) => {
            state.playerDataOne = action.payload
        },
        updatePlayerTwo: (state, action) => {
            state.playerDataTwo = action.payload
        },
    },
    extraReducers: (builder) => {
        builder.addCase(getWinner.pending, state => {
            state.loading = true;
        });

        builder.addCase(getWinner.fulfilled,
            (state, {payload}) => {
                    state.loading = false;
                    state.winner = payload[0];
                    state.loser = payload[1];

            },);

        builder.addCase(
            getWinner.rejected,
            (state, {payload}) => {
                state.loading  = false;
                state.winner = payload[0];
                state.loser = payload[1];
            },
        )
    },
})

export const {
    updatePlayerOne,
    updatePlayerTwo
} = battleSlice.actions

export default battleSlice.reducer;