// import {SET_PLAYER_DATA_ONE, SET_PLAYER_DATA_TWO, GET_WINNER, GET_LOSER, GET_PLAYER_LOADING} from "./battle.constant";
//
// const initialState = {
//     playerDataOne: {
//         playerOneName: "",
//         playerOneImage: null,
//     },
//     playerDataTwo: {
//         playerTwoName: "",
//         playerTwoImage: null
//     },
//     winner: {
//         profile: [],
//         score: null,
//     },
//     loser: {
//         profile: [],
//         score: null,
//     },
//     loading: true,
// }
//
// const battleReducer = (state = initialState, action) => {
//     switch (action.type) {
//         case SET_PLAYER_DATA_ONE:
//             return {
//                 ...state,
//                 playerDataOne: action.payload
//             }
//         case SET_PLAYER_DATA_TWO:
//             return {
//                 ...state,
//                 playerDataTwo: action.payload
//             }
//         case GET_PLAYER_LOADING:
//             return {
//                 ...state,
//                 loading: false
//             }
//         case GET_WINNER:
//             return {
//                 ...state,
//                 winner: action.payload
//             }
//         case GET_LOSER:
//             return {
//                 ...state,
//                 loser: action.payload
//             }
//         default:
//             return state;
//     }
// }
//
//
// export default battleReducer;