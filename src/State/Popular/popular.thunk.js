import {fetchPopularRepos} from "../../api/Api";
import {createAsyncThunk} from "@reduxjs/toolkit";
import {updateLanguage} from "./popular.slice";

// export const getRepos = (selectedLanguage) => (dispatch) => {
//
//     // dispatch(getReposLoadingAction)
//     dispatch(getReposPreLoadingAction)
//
//     fetchPopularRepos(selectedLanguage)
//         .then((data) => {
//             dispatch(getReposPreLoadingAction(data));
//             dispatch(getReposLoadingAction(data));
//             dispatch(getReposSuccessAction(data))
//         }
//     )
//         .catch(error => dispatch(getReposFailureAction))
// }


export const getRepos = createAsyncThunk(
    'popular/getRepos',
    async (selectedLanguage, {rejectWithValue},dispatch)=> {
        // dispatch(updateLanguage(selectedLanguage))
        try {
            return await fetchPopularRepos(selectedLanguage)
        } catch (error) {
            return rejectWithValue(error)
        }
}
)