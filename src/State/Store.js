import {configureStore, getDefaultMiddleware} from "@reduxjs/toolkit";
import popular from "./Popular/popular.slice";
import {createLogger} from "redux-logger"
import battle from "./Battle/battle.slice";




const store = configureStore({
    reducer: {
        popular,
        battle
    },
    middleware: () => getDefaultMiddleware().concat(
        createLogger({
            collapsed: true
        })
    )
})

export default store;